# Katana
Katana is a Dreamcast emulator that is created in Rust.
With inspiration from yupfarris with his N64 project called Rustendo64. (Check it out it's amazing!)

# How to use Katana
Currently there is no way to load files without using the command line and cargo run.
With cargo run, we gather 2 arguments from the command line, the first one being the BIOS and the second being the game of choice.

# RULES
We will never ever supply BIOS files or ISO files, we will only provide demo files.
The demo file for choice currently is 256B from http://www.boob.co.uk/demos.html