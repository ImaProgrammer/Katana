/* Memory
Start	    End	        Size	Description
0x00000000  0x1FFFFFFF  2M      BIOS Boot ROM. Reset entry point is at 0x00000000
0x00200000	0x0021FFFF	128K	Flash ROM
0x005F6000	0x005F7FFF	N/A	    ASIC registers (including GD-Rom and Maple control registers)
0x005F8000	0x005F8FFF	N/A	    PVR2 registers
0x005F9000	0x005F9FFF	4K	    PVR2 Palette
0x00700000	0x00710FFF	N/A	    AICA control registers
0x00800000	0x009FFFFF	2M	    Audio RAM access (via G2 FIFO)
0x04000000	0x047FFFFF	8M	    64-bit VRAM access
0x05000000	0x057FFFFF	8M	    32-bit VRAM access
0x0C000000	0x0CFFFFFF	16M	    Main RAM
0x10000000	0x107FFFFF	N/A	    Tile Accelerator FIFO (burst)
0x10800000	0x10FFFFFF	N/A	    YUV Decoder FIFO (burst)
0x11000000	0x11FFFFFF	N/A	    VRAM access 1 FIFO (burst)
0x12000000	0x127FFFFF	N/A	    Tile Accelerator FIFO (burst) repeat
0x12800000	0x12FFFFFF	N/A	    YUV Decoder FIFO (burst) repeat
0x13000000	0x13FFFFFF	N/A	    VRAM access 2 FIFO (burst)
0x1C000000	0x1FFFFFFF	N/A	    Access to CPU control registers in P4 region or when mapped via TLB. Reserved area in all other regions.
Source = http://www.lxdream.org/wiki/index.php?title=Memory_Map
*/

pub mod map {
    pub struct Range(u32, u32);
    
    impl Range {
        // Return Some(offset) if address is contained in self
        pub fn contains(self, address: u32) -> Option<u32> {
            let Range(start, length) = self;
            
            if address >= start && address < start + length {
                Some(address - start)
            } else {
                None
            }
        }
    }
    
    pub const BIOS: Range = Range(0x0000_0000, 2 * 1024 * 1024);
    pub const FLASH: Range = Range(0x0020_0000, 128 * 1024);
}
