// Our use cases
use super::super::interconnect;
use super::opcode::Opcode;

#[derive(Debug)]
pub struct Cpu {
    gpr_regs: [u32; 16],
    fpr_regs: [f32; 32],
    
    reg_sr: u32,    // Status register
    reg_gbr: u32,   // Global base register
    reg_ssr: u32,   // Saved status register
    reg_spc: u32,   // Stack pointer
    reg_sgr: u32,   // Saved general register
    reg_dbr: u32,   // Debug base register
    reg_vbr: u32,   // Vector base register
    reg_mach: u32,  // Multiply-and-accumlate register high
    reg_macl: u32,  // Multiply-and-accumlate register low
    reg_pr: u32,    // Procedure register
    reg_fpul: u32,  // Floating-point communication register
    reg_pc: u32,    // Program counter
    reg_fpscr: u32, // Floating-point status/control register
    
    pub interconnect: interconnect::Interconnect,
}

impl Cpu {
    pub fn new(interconnect: interconnect::Interconnect) -> Self {
        Cpu {
            gpr_regs: [0; 16],
            fpr_regs: [0.0; 32],
    
            reg_sr: 0,
            reg_gbr: 0,
            reg_ssr: 0,
            reg_spc: 0,
            reg_sgr: 0,
            reg_dbr: 0,
            reg_vbr: 0x0000_0000,
            reg_mach: 0,
            reg_macl: 0,
            reg_pr: 0,
            reg_fpul: 0,
            reg_pc: 0x0000_0000,
            reg_fpscr: 0x0004_0001,
            
            interconnect: interconnect,
        }
    }
    
    pub fn run_fetch_instruction(&mut self) {
        let pc = self.reg_pc;
        
        // Fetch instruction at reg_pc
        let instruction = self.load_u32(pc);
        // Increment reg_pc to point at the next instruction
        self.reg_pc = pc.wrapping_add(2);
        // Execute the instruction
        self.execute_instruction(instruction);
    }
    
    pub fn execute_instruction(&mut self, instruction: u32) {
        let opcode = (instruction >> 10) & ((1 << 19) - 1);
        
        match opcode {
            // TODO
            _ => panic!("Unhandled instruction: {:#x} (opcode: {:#b}", instruction, opcode),
        }
    }
    
    pub fn power_on_reset(&mut self) {
        // TODO
        self.reg_vbr = 0x0000_0000;
        self.reg_pc = 0xA000_0000;
    }
    
    pub fn manual_reset(&mut self) {
        // TODO
        self.reg_vbr = 0x0000_0000;
        self.reg_pc = 0xA000_0000;
    }
    
    fn load_u32(&self, address: u32) -> u32 {
        self.interconnect.load_u32(address)
    }
}
