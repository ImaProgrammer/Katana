use std::*;
use super::sh4;
use super::interconnect;
use super::bios;

#[derive(Debug)]
pub struct Dreamcast {
    pub cpu: sh4::cpu::Cpu,
}

impl Dreamcast {
    pub fn new(bios: bios::Bios) -> Self {
        let interconnect = interconnect::Interconnect::new(bios);
        let cpu = sh4::cpu::Cpu::new(interconnect);
        
        Dreamcast {
            cpu: cpu,
        }
    }
    
    pub fn run(&mut self) {
        loop {
            self.cpu.run_fetch_instruction();
        }
    }
    
    pub fn power_on_reset(&mut self) {
        self.cpu.power_on_reset();
    }
    
    pub fn manual_reset(&mut self) {
        self.cpu.manual_reset();
    }
}