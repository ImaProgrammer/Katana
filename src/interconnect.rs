extern crate byteorder;

use super::bios;
use super::memory;
use byteorder::{BigEndian, LittleEndian, ByteOrder};

const SYSTEM_RAM_SIZE: usize = 16 * 1024 * 1024;
const VIDEO_RAM_SIZE: usize = 8 * 1024 * 1024;
const AUDIO_RAM_SIZE: usize = 2 * 1024 * 1024;

const FLASH_ROM_SIZE: usize = 128 * 1024;

#[derive(Debug)]
pub struct Interconnect {
    pub dram: Box<[u8]>,   // System ram
    pub gddr: Box<[u32]>,   // Video ram
    pub addr: Box<[u32]>,   // Audio ram
    
    // Basic Input and Output memory
    bios: bios::Bios,
    iso: Vec<u8>,
}

impl Interconnect {
    pub fn new(bios: bios::Bios) -> Self {
        Interconnect {
            dram: vec![0; SYSTEM_RAM_SIZE].into_boxed_slice(),
            gddr: vec![0; VIDEO_RAM_SIZE].into_boxed_slice(),
            addr: vec![0; AUDIO_RAM_SIZE].into_boxed_slice(),
            
            bios: bios,
            iso: Vec::new(),
        }
    }
    
    // Read u32 word at address
    pub fn read_u32(&self, address: u32) -> u32 {
        BigEndian::read_u32(&self.dram[address as usize..])
    }
    
    // Load u32 word at address
    pub fn load_u32(&self, address: u32) -> u32 {
        if let Some(offset) = memory::map::BIOS.contains(address) {
            return self.bios.load_u32(offset);
        }
        
        panic!("Unhandled load_u32 at address: {:08x}", address);
    }
}