// Katana (Dreamcast Emulator)(version 0.0.1)

extern crate byteorder;
// Our internal crates
mod dreamcast;
mod bios;
mod interconnect;
mod sh4;
mod memory;

use std::env;
use std::path;

fn main() {
    // Generate an array of strings from cargo arguments
    let args: Vec<String> = env::args().collect();
    // We read the BIOS location that the user has supplied us
    let bios_path = path::Path::new(&args[1]);
    let bios = bios::Bios::new(&bios_path).unwrap();
    // Declare the emulator
    let mut emulator = dreamcast::Dreamcast::new(bios);
    
    println!("BIOS Loaded: {:?}", &bios_path);
    
    // Launch the emulator
    emulator.run();
    
    //println!("{:#?}", &emulator);
}
