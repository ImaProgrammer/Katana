extern crate byteorder;

use std::*;
use std::io::Read;
use byteorder::{BigEndian, LittleEndian, ByteOrder};

const BIOS_ROM_SIZE: u64 = 2 * 1024 * 1024;

// BIOS image
#[derive(Debug)]
pub struct Bios {
    // BIOS memory
    data: Vec<u8>,
}

impl Bios {
    // Load a BIOS image from the file located at path
    pub fn new(file_path: &path::Path) -> Result<Bios, io::Error> {
        let file = try!(fs::File::open(file_path));
        let mut data = Vec::new();
        
        // Load the BIOS
        try!(file.take(BIOS_ROM_SIZE).read_to_end(&mut data));
        
        if data.len() == BIOS_ROM_SIZE as usize {
            Ok(Bios {data: data})
        } else {
            Err(io::Error::new(io::ErrorKind::InvalidInput, "Invalid BIOS size"))
        }
    }
    
    pub fn load_u32(&self, offset: u32) -> u32 {
        BigEndian::read_u32(&self.data[offset as usize..])
    }
}